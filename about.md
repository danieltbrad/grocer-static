---
layout: page
title: About
permalink: /about/
---

Grocer is a simple but powerful tool that lets New Seasons Market customers stay on top of their shopping habits (and budget). Want to know how many times you purchased a particular product? If its price has changed over time? What you spend the most on? It's all there, in easy to search tables and helpful charts.

Grocer parses your New Seasons Market receipts, and from them creates a rich database of your shopping trips, products, prices over time, and other analytics that will help you get a handle on your purchasing patterns. The only thing Grocer needs are your shopping receipt emails. Manually forward them to intake@receipts.my-grocer.com or better yet, set up your email provider to automatically forward your receipts for you. It's easy to set up a forwarding filter with Gmail (click here for a two minute walkthrough video), and many other email providers allow it too.

Even if you don't import your purchase history, you can still use Grocer. I've leveraged the existing product database into a "smart" shopping list maker that lets you easily and quickly build shopping lists with built-in price information. There are 4052 products in the database and it's growing every week. So come on in and check it out. For more detailed information about how Grocer works, click here
